module Queries
  class FetchItems < Queries::BaseQuery
    type [Types::ItemType], null: false
    def resolve
      Item.all.order(created_at: :desc)
    end
  end

  class FetchItem < Queries::BaseQuery
    type Types::ItemType, null: false
    argument :id, ID, required: true

    def resolve(id:)
      Item.find(id)
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Item does not exist.')
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:/ #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
