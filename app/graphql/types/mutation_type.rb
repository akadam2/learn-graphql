module Types
  class MutationType < Types::BaseObject
    # TODO: remove me
    field :test_field, String, null: false,
      description: "An example field added by the generator"

    field :add_note, mutation: Mutations::AddNote
    field :destroy_note, mutation: Mutations::DestroyNote
    # field :show_note, mutation: Mutations::ShowNote
    field :update_note, mutation: Mutations::UpdateNote

    def test_field
      "Hello World"
    end
  end
end
