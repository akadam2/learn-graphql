module Mutations
  class DestroyNote < Mutations::BaseMutation
    type Types::NoteType
    argument :id, Integer, required: true

    def resolve(id:)
      Note.find(id).destroy
    end
  end
end