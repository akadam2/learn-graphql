module Mutations
  class UpdateNote < Mutations::BaseMutation
    argument :id, Integer, required: true
    argument :title, String, required: false
    argument :body, String, required: false

    type Types::NoteType

    def resolve(id:, **attributes)
      Note.find(id).tap do |note|
        note.update!(attributes)
      end
    end
  end
end